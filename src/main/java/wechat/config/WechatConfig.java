package wechat.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import wechat.utils.WechatBotClient;

import java.net.URISyntaxException;

/**
 * 实例化bean
 */
@Configuration
public class WechatConfig {

    /**
     * 微信链接地址，具体地址为http://X.X.X.X:5555
     */
    @Value("${wechat.url}")
    private String wechatBotUrl;

    /**
     * 初始化
     */
    @Bean
    public WechatBotClient initWechatBotClient() throws URISyntaxException {
        //创建一个微信客户端
        WechatBotClient botClient = new WechatBotClient(wechatBotUrl);
        // 和本地微信创建连接
        botClient.connect();
        return botClient;
    }

}
