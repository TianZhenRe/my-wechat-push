package wechat.wechatBot.domain;

import lombok.Data;

/**
 * 消息实体类
 */
@Data
public class WechatMsg {
    /**
     * 消息id
     */
    private String id;
    /**
     * 接收消息人的 微信原始id
     */
    private String wxid;
    /**
     * 消息内容、图片路径、文件路径（必须是本地的）
     */
    private String content;
    /**
     * 群组id 群组内发送@消息时使用
     */
    private String roomid;
    /**
     * 昵称 群组内发送@消息时使用，为被@昵称
     */
    private String nickname;
    /**
     * 发送消息类型
     */
    private Integer type;

    private String path;

    private String ext;

}
