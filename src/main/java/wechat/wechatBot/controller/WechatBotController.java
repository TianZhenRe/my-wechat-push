package wechat.wechatBot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import wechat.utils.AjaxResult;
import wechat.utils.WechatBotCommon;
import wechat.wechatBot.domain.WechatMsg;
import wechat.wechatBot.service.WechatBotService;

/**
 * 调用方法
 */
@RestController
public class WechatBotController implements WechatBotCommon {

    @Autowired
    private WechatBotService wechatBotService;


    /**
     * 通用接口，发送消息类型需要自己配置
     *
     * @return
     */
    @RequestMapping("/wechatCommon")
    public AjaxResult wechatCommon() {
        WechatMsg wechatMsg = new WechatMsg();
        wechatMsg.setType(PIC_MSG);
        wechatMsg.setWxid("21902614672@chatroom");
        wechatMsg.setContent("D:\\ig.jpg");
        wechatMsg.setPath("");

        wechatMsg.setRoomid("");
        wechatMsg.setExt("");
        wechatMsg.setNickname("xiaobai");

        wechatBotService.wechatCommon(wechatMsg);
        return AjaxResult.success();
    }


    /**
     * 发送文本消息（可单人，可群组）
     *
     * @return
     */
    @RequestMapping("/sendTextMsg")
    public AjaxResult sendTextMsg() {
        WechatMsg wechatMsg = new WechatMsg();
        wechatMsg.setWxid("21902614672@chatroom");
        wechatMsg.setContent("xxx站点，2021-08-13 13：00 二氧化硫超标。");

        wechatBotService.sendTextMsg(wechatMsg);
        return AjaxResult.success();
    }

    /**
     * 群组内发送@指定人消息(dll 3.1.0.66版本不可用)
     *
     * @return
     */
    @RequestMapping("/sendATMsg")
    public AjaxResult sendATMsg() {
        WechatMsg wechatMsg = new WechatMsg();
        wechatMsg.setNickname("小白");
        wechatMsg.setRoomid("21902614672@chatroom");
        wechatMsg.setContent("哈哈哈");

        wechatBotService.sendATMsg(wechatMsg);
        return AjaxResult.success();
    }

    /**
     * 发送图片消息
     *
     * @return
     */
    @RequestMapping("/sendImgMsg")
    public AjaxResult sendImgMsg() {

        WechatMsg wechatMsg = new WechatMsg();
        wechatMsg.setWxid("21902614672@chatroom");
        wechatMsg.setContent("D:\\ig.jpg");
        // 发送消息
        wechatBotService.sendImgMsg(wechatMsg);
        return AjaxResult.success();

    }


    /**
     * 发送附件
     *
     * @return
     */
    @RequestMapping("/sendAnnex")
    public AjaxResult sendAnnex() {
        WechatMsg wechatMsg = new WechatMsg();
        wechatMsg.setWxid("21902614672@chatroom");
        wechatMsg.setRoomid("21902614672@chatroom");
        wechatMsg.setContent("D:\\ig.jpg");

        wechatBotService.sendAnnex(wechatMsg);
        return AjaxResult.success();
    }


    /**
     * 获取微信群组,联系人列表
     *
     * @return
     */
    @GetMapping("/getWeChatUserList")
    public AjaxResult getWeChatUserList() {
        wechatBotService.getWeChatUserList();
        //控制台输出
        //可以去WechatBotClient，根据返回的s的type，获取列表
        return AjaxResult.success();
    }

    /**
     * 获取个人详细信息 3.2.2.121版本dll 未提供该接口
     *
     * @return
     */
    @GetMapping("getPersonalDetail")
    public AjaxResult getPersonalDetail() {
        String wxid = "wxid_pmlx31ml21t422";
        wechatBotService.getPersonalDetail(wxid);
        return AjaxResult.success();
    }
}
