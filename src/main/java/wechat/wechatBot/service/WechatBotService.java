package wechat.wechatBot.service;


import wechat.wechatBot.domain.WechatMsg;

/**
 * @author: [青衫] 'QSSSYH@QQ.com'
 * @Date: 2021-03-18 20:55
 * @Description: < 描述 >
 */
public interface WechatBotService {

    /**
     * 发送文字消息
     *
     * @param wechatMsg
     */
    public void wechatCommon(WechatMsg wechatMsg);

    /**
     * 发送文字消息
     *
     * @param wechatMsg
     */
    public void sendTextMsg(WechatMsg wechatMsg);

    /**
     * 发送图片消息
     *
     * @param wechatMsg
     */
    public void sendImgMsg(WechatMsg wechatMsg);

    /**
     * 群组内发送@指定人消息
     *
     * @param wechatMsg
     */
    void sendATMsg(WechatMsg wechatMsg);


    /**
     * 发送附件
     *
     * @param wechatMsg
     */
    void sendAnnex(WechatMsg wechatMsg);

    /**
     * 获取微信群组,联系人列表
     */
    void getWeChatUserList();

    /**
     * 获取指定联系人的详细信息
     *
     * @param wxid
     */
    void getPersonalDetail(String wxid);

}
