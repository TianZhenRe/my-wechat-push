package wechat.utils;

import com.alibaba.fastjson.JSONObject;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.springframework.util.StringUtils;
import wechat.wechatBot.domain.WechatMsg;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * 微信发送消息工具类
 */
public class WechatBotClient extends WebSocketClient implements WechatBotCommon {

    /**
     * 构造方法创建 WechatBotClient对象
     *
     * @param url
     * @throws URISyntaxException
     */
    public WechatBotClient(String url) throws URISyntaxException {
        super(new URI(url));
    }

    /**
     * 在websocket连接开启时，触发的提示信息
     *
     * @param serverHandshake
     */
    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        System.err.println("已发送尝试连接到微信客户端请求");
    }

    /**
     * 别人给你发微信消息时，触发的提示信息
     *
     * @param s
     */
    @Override
    public void onMessage(String s) {
        System.out.println("微信中收到了消息:" + s);
    }

    /**
     * 微信连接断开时，触发的提示信息
     *
     * @param i
     * @param s
     * @param b
     */
    @Override
    public void onClose(int i, String s, boolean b) {
        System.out.println("已断开连接... ");
    }

    /**
     * 微信连接出错时，触发的提示信息
     *
     * @param e
     */
    @Override
    public void onError(Exception e) {
        System.err.println("通信连接出现异常:" + e.getMessage());

    }

    /**
     * 发送消息
     *
     * @param wechatMsg
     */
    public void sendMsgUtil(WechatMsg wechatMsg) {
        if (!StringUtils.hasText(wechatMsg.getExt())) {
            wechatMsg.setExt(NULL_MSG);
        }
        if (!StringUtils.hasText(wechatMsg.getNickname())) {
            wechatMsg.setNickname(NULL_MSG);
        }
        if (!StringUtils.hasText(wechatMsg.getRoomid())) {
            wechatMsg.setRoomid(NULL_MSG);
        }
        if (!StringUtils.hasText(wechatMsg.getContent())) {
            wechatMsg.setContent(NULL_MSG);
        }
        if (!StringUtils.hasText(wechatMsg.getWxid())) {
            wechatMsg.setWxid(NULL_MSG);
        }


        // 消息Id
        wechatMsg.setId(String.valueOf(System.currentTimeMillis()));
        // 发送消息转为json字符串
        String string = JSONObject.toJSONString(wechatMsg);
        // 发送消息
        send(JSONObject.toJSONString(wechatMsg));
    }
}
